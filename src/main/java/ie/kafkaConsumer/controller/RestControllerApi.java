package ie.kafkaConsumer.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ie.kafkaConsumer.entities.Topic;
import ie.kafkaConsumer.services.DataService;
import ie.kafkaConsumer.services.KafkaProducerService;

@RestController
@RequestMapping("/api")
public class RestControllerApi {
	private static Logger logger = LoggerFactory.getLogger(RestControllerApi.class);
	
	@Autowired
	DataService dataService;

	@Autowired
	KafkaProducerService kafkaProducerService;
	
	@GetMapping("/sensorData")
	public List<Topic> getData() {
		
		List<Topic> allData = dataService.getData();
		logger.info("inApi----------------\n-----------------\n---------------");
		logger.info("allData size" + Integer.toString(allData.size()));
		return allData;		
	}
	
	@GetMapping("/sensorData/{id}")
	public List<Topic> getData(@PathVariable String id) {
		
		List<Topic> allData = dataService.getDataById(id);
		logger.info("inApiById----------------\n-----------------\n---------------");
		logger.info("allData size" + Integer.toString(allData.size()));
		return allData;		
	}
	
	@GetMapping("/sensorStatus/{id}")
	public List<Topic> getStatus(@PathVariable String id){
		String topicName = id + "_status"; 
		List<Topic> allData = dataService.getDataById(topicName);
		return allData;	
	}
	
	@GetMapping("/allSensors/")
	public List<Topic> getAllSensors() {
		List<Topic> allSensors = dataService.getAllSensors();
		return allSensors;
	}
	
	//this needs to be finished or deleted-------------------------------------------!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	@PostMapping("/head")
	public void head(@RequestBody Object topic) {
		//for(Object reading: theHead) {
		//}
		logger.info("in /head, Restcontroller " + topic.toString());
		kafkaProducerService.produce(topic);
	}
	
	@PostMapping("/status")
	public void setStatus(@RequestBody Object topic) {
		logger.info("in /status, Restcontroller " + topic.toString());
		kafkaProducerService.produce(topic);
	}
}
