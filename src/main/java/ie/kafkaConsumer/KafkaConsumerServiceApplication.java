package ie.kafkaConsumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import ie.kafkaConsumer.controller.RestControllerApi;

@SpringBootApplication
public class KafkaConsumerServiceApplication {

//	@Autowired
//	RestControllerApi restControllerApi;
	
	public static void main(String[] args) {
		SpringApplication.run(KafkaConsumerServiceApplication.class, args);
	}

}
