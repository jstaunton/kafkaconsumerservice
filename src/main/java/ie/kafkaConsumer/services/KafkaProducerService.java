package ie.kafkaConsumer.services;

import java.util.List;

public interface KafkaProducerService {

	void produce(Object aTopic);
}
