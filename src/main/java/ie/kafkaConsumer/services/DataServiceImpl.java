package ie.kafkaConsumer.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ie.kafkaConsumer.Config.ConsumerCreator;
import ie.kafkaConsumer.Config.IKafkaConstants;
import ie.kafkaConsumer.entities.Topic;

@Service
public class DataServiceImpl implements DataService {
	private static Logger logger = LoggerFactory.getLogger(DataServiceImpl.class);

	@Override
	public List<Topic> getData() {
		Consumer<Long, String> consumer = ConsumerCreator.createConsumer();
		int noMessageFound = 0;
		List<Topic> values = new ArrayList<Topic>();
		int counter = 0;
		try {
			while (true) {
				ConsumerRecords<Long, String> consumerRecords = consumer.poll(1000);
				if (consumerRecords.count() == 0) {
					noMessageFound++;
					if (noMessageFound > IKafkaConstants.MAX_NO_MESSAGE_FOUND_COUNT)
						break;
					else
						continue;
				}
				counter += 1;
				logger.info("counter " + Integer.toString(counter));
				consumerRecords.forEach(record -> {
					Topic topic = new Topic();
					//topic.setName("test8");
					topic.setName(record.topic());
					topic.setData(record.value());
					values.add(topic);
				});
			}
		} catch (WakeupException e) {
			logger.info("error: " + e.getMessage());
		} finally {
			consumer.commitSync();
			consumer.close();
		}

		return values;
	}

	@Override
	public List<Topic> getDataById(String id) {
		Consumer<Long, String> consumer = ConsumerCreator.createANamedConsumer(id);
		int noMessageFound = 0;
		List<Topic> values = new ArrayList<Topic>();
		int counter = 0;
		try {
			while (true) {
				ConsumerRecords<Long, String> consumerRecords = consumer.poll(1000);
				if (consumerRecords.count() == 0) {
					noMessageFound++;
					if (noMessageFound > IKafkaConstants.MAX_NO_MESSAGE_FOUND_COUNT)
						break;
					else
						continue;
				}
				counter += 1;
				logger.info("counter " + Integer.toString(counter));
				consumerRecords.forEach(record -> {
					Topic topic = new Topic();
					topic.setName(record.topic());
					topic.setData(record.value());
					values.add(topic);
				});
//				if(counter == 1) {
//					break;
//				}
			}
		} catch (WakeupException e) {
			logger.info("error: " + e.getMessage());
		} finally {
			consumer.commitSync();
			consumer.close();
		}

		return values;
	}

	@Override
	public List<Topic> getAllSensors() {
		Map<String, List<PartitionInfo> > topics;
		Consumer<Long, String> consumer = ConsumerCreator.createNonTopicConsumer();
		
		topics = consumer.listTopics();
//		logger.info("topics entryset " + topics.entrySet());
//		logger.info("\n");
		logger.info("topics keyset" + topics.keySet());
		Set<String> keySet = topics.keySet();
		List<Topic> values = new ArrayList<Topic>();
		for (String key: keySet) {
			if(!key.substring(key.length() - 6).equals("status") && !key.contains("offsets")) {
				logger.info("topics key " + key);
				consumer.subscribe(Collections.singletonList(key));
				int noMessageFound = 0;
				int counter = 0;
				try {
					while (true) {
						ConsumerRecords<Long, String> consumerRecords = consumer.poll(1000);
						if (consumerRecords.count() == 0) {
							noMessageFound++;
							if (noMessageFound > IKafkaConstants.MAX_NO_MESSAGE_FOUND_COUNT)
								break;
							else
								continue;
						}
						counter += 1;
						logger.info("counter " + Integer.toString(counter));
						consumerRecords.forEach(record -> {
							Topic topic = new Topic();
							topic.setName(record.topic());
							topic.setData(record.value());
							values.add(topic);
							logger.info("topic " + topic.getName() + " " + topic.getData());
						});
//						if(counter == 1) {
//							break;
//						}
					}
				} catch (WakeupException e) {
					logger.info("error: " + e.getMessage());
				} finally {
					consumer.commitSync();
				}
			}
		}
		consumer.close();
		for (Topic value: values) {
			logger.info("value: " + value.getName() + " " + value.getData());
			
		}
		return values;
		
	}
}
