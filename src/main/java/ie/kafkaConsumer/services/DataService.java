package ie.kafkaConsumer.services;

import java.util.List;

import ie.kafkaConsumer.entities.Topic;

public interface DataService {

	List<Topic> getData();
	List<Topic> getDataById(String id);
	List<Topic> getAllSensors();
}
