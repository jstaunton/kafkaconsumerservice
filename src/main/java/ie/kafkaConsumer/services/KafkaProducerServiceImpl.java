package ie.kafkaConsumer.services;

import java.util.LinkedHashMap;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ie.kafkaConsumer.Config.IKafkaConstants;
import ie.kafkaConsumer.Config.ProducerCreator;

@Service
public class KafkaProducerServiceImpl implements KafkaProducerService {
	private static Logger logger = LoggerFactory.getLogger(KafkaProducerServiceImpl.class);
	Producer<Long, String> producer = ProducerCreator.createProducer();

	@Override
	public void produce(Object aTopic) {

		logger.info("in produce(), KPSImpl *****" + aTopic.toString());
		LinkedHashMap<String, String> lhm = (LinkedHashMap<String, String>) aTopic;
		String topicName = (String) lhm.get("name");
		String message = (String) lhm.get("data");
		String status = lhm.get("status");

		logger.info(topicName);
		logger.info(message);
		ProducerRecord<Long, String> record = new ProducerRecord<Long, String>(topicName, status + " " + message);
		try {
			RecordMetadata metadata = producer.send(record).get();
			System.out.println("Record sent with key " + metadata.topic() + " to partition " + metadata.partition()
					+ " with offset " + metadata.offset() + " to topic " + metadata.topic());
		} catch (ExecutionException e) {
			System.out.println("Error in sending record");
			System.out.println(e);
		} catch (InterruptedException e) {
			System.out.println("Error in sending record");
			System.out.println(e);
		}
	}
}
